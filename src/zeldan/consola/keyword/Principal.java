package zeldan.consola.keyword;

import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.opencsv.CSVWriter;


public class Principal {

	public static void main(String[] args) {
		
		

		
		List<Peli> keylist = getMoviesKeywords();
		System.out.println(keylist.size());

		HashSet<Keywords> listaDepurada = new HashSet<Keywords>();
		String csvFile = "/Users/Grupo1/key_movies.csv";
		String csvFile2 = "/Users/Grupo1/key.csv";
		try {
			FileWriter archivo = new FileWriter(csvFile);

			CSVWriter writer = new CSVWriter(archivo, ',');
			// feed in your array (or convert your data to an array)

			for (Peli p : keylist) {
				String id = Integer.toString(p.getIdPeli());
				
				for (Keywords k : p.getKeywords()) {

					String id2 = Integer.toString(k.getId());

					String[] entries = { id, id2 };
					writer.writeNext(entries);
					listaDepurada.add(k);
				}
			}
			writer.close();
			
			FileWriter archivo2 = new FileWriter(csvFile2);
			CSVWriter writer2 = new CSVWriter(archivo2, ',');
			for (Keywords k : listaDepurada) {
				String name = k.getName();
				String id2 = Integer.toString(k.getId());

				String[] entries = { id2, name };
				writer2.writeNext(entries);

			}
			writer2.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("eerro archivo");
		}

		System.out.println("FIN -----------------------------------------------");
	}

	public static List<Peli> getMoviesKeywords() {

		final String TABLE = "moviekeywords";
		List<Peli> moviesList = new ArrayList<Peli>();

		String sql = String.format("SELECT id,keyword  FROM %s", TABLE);

		try (Connection conn = DBCon.getConn(); Statement stmt = conn.createStatement()) {

			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				Peli m = new Peli(rs.getInt(1), getKeyDeJson(rs.getString(2)));
				moviesList.add(m);
			}

		} catch (Exception e) {

			String s = e.getMessage();
			System.out.println(s);

		}
		return moviesList;
	}

	public static List<Keywords> getKeyDeJson(String json) {

		List<Keywords> keyList = new ArrayList<Keywords>();

		

			Gson gs = new Gson();

			List<Keywords> listaTemporal = gs.fromJson(json, new TypeToken<List<Keywords>>() {
			}.getType());

			for (Keywords g : listaTemporal) {

				keyList.add(g);
			}

		
		return keyList;
	
	}

}
