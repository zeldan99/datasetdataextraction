package zeldan.consola.keyword;

import java.util.List;

public class Peli {
	private int idPeli;
	private List<Keywords> k;
	
	/**
	 * @param idPeli
	 * @param k
	 */
	public Peli(int idPeli, List<Keywords> k) {
		this.idPeli = idPeli;
		this.k = k;
	}
	public int getIdPeli() {
		return idPeli;
	}
	public void setIdPeli(int idPeli) {
		this.idPeli = idPeli;
	}
	public List<Keywords> getKeywords() {
		return k;
	}
	public void setKeywords(List<Keywords> k) {
		this.k = k;
	}
	

}
