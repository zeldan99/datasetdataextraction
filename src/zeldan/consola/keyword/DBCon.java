package zeldan.consola.keyword;


import java.sql.DriverManager;
import java.sql.SQLException;

import com.mysql.jdbc.Connection;

public class DBCon {

	private static final String URL = "jdbc:mysql://localhost/bd_pelis";
	private static final String USERNAME = "root";
	private static final String PASSWORD = "";
	

	public static Connection getConn() throws SQLException {
		DriverManager.registerDriver(new com.mysql.jdbc.Driver ());
		return (Connection) DriverManager.getConnection(URL, USERNAME, PASSWORD);
	}

}
