package zeldan.consola.keyword;


public class Keywords {

	private int id;
	private String name;
	/**
	 * @param idKeyword
	 * @param name
	 */
	public Keywords(int id, String name) {
		this.id = id;
		this.name = name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public boolean equals(Object obj) {
		 if (obj instanceof Keywords) {
			 Keywords p = (Keywords)obj;
			    return this.id==p.id;
			  } else {
			    return false;
			  }
	}
	@Override
	public int hashCode() {
	  return this.id;
	}
}
